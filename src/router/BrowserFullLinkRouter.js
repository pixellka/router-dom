import * as React from "react";
// import { createBrowserHistory as createAlternateHistory } from "./AlternateHistory.tsx";
import { createBrowserHistory as createAltHistory, createMemoryHistory} from "./AltHistory.js";
import { createRouter, createLocation, createPath, getUrlBasedHistory, createBrowserHistory } from '@remix-run/router';
// import { BrowserRouter } from "react-router-dom";
// import type { createBrowserHistory, BrowserHistory, HashHistory, State, To } from "history";

import {
    // createBrowserRouter,
    AbortedDeferredError, Await, Form, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, RouterProvider, Routes, ScrollRestoration, UNSAFE_DataRouterContext, UNSAFE_DataRouterStateContext, UNSAFE_DataStaticRouterContext, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, UNSAFE_enhanceManualRouteObjects, createHashRouter, createMemoryRouter, createRoutesFromChildren, createRoutesFromElements, createSearchParams, defer, generatePath, isRouteErrorResponse, json, matchPath, matchRoutes, parsePath, redirect, renderMatches, resolvePath, unstable_HistoryRouter, useActionData, useAsyncError, useAsyncValue, useFetcher, useFetchers, useFormAction, useHref, useInRouterContext, useLinkClickHandler, useLoaderData, useLocation, useMatch, useMatches, useNavigate, useNavigation, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRevalidator, useRouteError, useRouteLoaderData, useRoutes, useSearchParams, useSubmit  
} from "react-router-dom";
// Note: Keep in sync with react-router exports!
export {
    // createBrowserRouter,
    AbortedDeferredError, Await, Form, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, RouterProvider, Routes, ScrollRestoration, UNSAFE_DataRouterContext, UNSAFE_DataRouterStateContext, UNSAFE_DataStaticRouterContext, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, UNSAFE_enhanceManualRouteObjects, createHashRouter, createMemoryRouter, createRoutesFromChildren, createRoutesFromElements, createSearchParams, defer, generatePath, isRouteErrorResponse, json, matchPath, matchRoutes, parsePath, redirect, renderMatches, resolvePath, unstable_HistoryRouter, useActionData, useAsyncError, useAsyncValue, useFetcher, useFetchers, useFormAction, useHref, useInRouterContext, useLinkClickHandler, useLoaderData, useLocation, useMatch, useMatches, useNavigate, useNavigation, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRevalidator, useRouteError, useRouteLoaderData, useRoutes, useSearchParams, useSubmit  
};
  





export function createBrowserRouter(routes, opts) {
    var _window;
    // console.log("createBrowserRouter", routes, opts);
    return createRouter({
      basename: opts == null ? 0 : opts.basename,
      history: createAltHistory({
        window: opts == null ? 0 : opts.window}, opts.argumentName??'path',
      ),
      hydrationData: (opts == null ? 0 : opts.hydrationData) || ((_window = window) == null ? 0 : _window.__staticRouterHydrationData),
      routes: UNSAFE_enhanceManualRouteObjects(routes)
    }).initialize();
}

//   /**
//    * A <Router> for use in web browsers. Provides the cleanest URLs.
//    */
export function BrowserRouter({ children, window, argumentName }) {
    let historyRef = React.useRef();
    if (historyRef.current == null) {
        historyRef.current = createAltHistory({ window, argumentName: argumentName??'path' });
    }
  
    let history = historyRef.current;
    let [state, setState] = React.useState({
      action: history.action,
      location: history.location 
    });

    React.useLayoutEffect(() => history.listen(setState), [history]);
  
    return (
      <Router
        children={children}
        action={state.action}
        location={state.location}
        navigator={history} 
      />
    );
}