import * as React from "react";
// import { createBrowserHistory as createAlternateHistory } from "./AlternateHistory.tsx";
// import { createBrowserHistory as createAltHistory, createMemoryHistory} from "./AltHistory.js";
import { createRouter, createLocation, createPath, getUrlBasedHistory, createBrowserHistory, createMemoryHistory } from '@remix-run/router';
// import { BrowserRouter } from "react-router-dom";
// import type { createBrowserHistory, BrowserHistory, HashHistory, State, To } from "history";

import {
    // createBrowserRouter,
    AbortedDeferredError, Await, Form, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, RouterProvider, Routes, ScrollRestoration, UNSAFE_DataRouterContext, UNSAFE_DataRouterStateContext, UNSAFE_DataStaticRouterContext, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, UNSAFE_enhanceManualRouteObjects, createHashRouter, createMemoryRouter, createRoutesFromChildren, createRoutesFromElements, createSearchParams, defer, generatePath, isRouteErrorResponse, json, matchPath, matchRoutes, parsePath, redirect, renderMatches, resolvePath, unstable_HistoryRouter, useActionData, useAsyncError, useAsyncValue, useFetcher, useFetchers, useFormAction, useHref, useInRouterContext, useLinkClickHandler, useLoaderData, useLocation, useMatch, useMatches, useNavigate, useNavigation, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRevalidator, useRouteError, useRouteLoaderData, useRoutes, useSearchParams, useSubmit  
} from "react-router-dom";
// Note: Keep in sync with react-router exports!
export {
    // createBrowserRouter,
    AbortedDeferredError, Await, Form, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, RouterProvider, Routes, ScrollRestoration, UNSAFE_DataRouterContext, UNSAFE_DataRouterStateContext, UNSAFE_DataStaticRouterContext, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, UNSAFE_enhanceManualRouteObjects, createHashRouter, createMemoryRouter, createRoutesFromChildren, createRoutesFromElements, createSearchParams, defer, generatePath, isRouteErrorResponse, json, matchPath, matchRoutes, parsePath, redirect, renderMatches, resolvePath, unstable_HistoryRouter, useActionData, useAsyncError, useAsyncValue, useFetcher, useFetchers, useFormAction, useHref, useInRouterContext, useLinkClickHandler, useLoaderData, useLocation, useMatch, useMatches, useNavigate, useNavigation, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRevalidator, useRouteError, useRouteLoaderData, useRoutes, useSearchParams, useSubmit  
};
  

const replaceUrlParam = (url, paramName, paramValue) =>
{
    if (paramValue == null) {
        paramValue = '';
    }
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
    if (url.search(pattern)>=0) {
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    url = url.replace(/[?#]$/,'');
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
}

const historyListener = (data, argumentName)=> {
    // console.log("listen: ", data, 'argumentName: ', argumentName);
    //replace url search param with data.location.pathname
    let href = window.location.href;
    //if url string does not contain string argumentName=, add it
    if (!href.includes(argumentName+"=")) {
        href += (href.includes("?") ? "&" : "?") + argumentName + "=" + data.location.pathname;
    } else {
        href = replaceUrlParam(href, argumentName, data.location.pathname);
    }
 
    console.log("href: ", href);



    // //split url
    // const hrefSplit = window.location.href.split(argumentName+"=");
    // // console.log("hrefSplit: ", hrefSplit);


    // const hrefPrefix = hrefSplit[0];
    // const hrefSuffix = hrefSplit[1]?.split("&")[1]??null;
    // href = hrefPrefix + argumentName + "=" + data.location.pathname + (hrefSuffix ? "&"+hrefSuffix : "");
    // // console.log("href: ", href);

    switch (data.action) {
        case "PUSH":
            window.history.pushState({}, '', href);
            break;
        case "REPLACE":
            window.history.replaceState({}, '', href);
            break;
        case "POP":
            console.log("createBrowserParamHistory: history.listen: POP: ", data);
            window.history.replaceState({}, '', href);
            break;
    }
}

const createBrowserParamHistory = (argumentName) => {
    // console.log("createBrowserParamHistory: ", argumentName);
    argumentName = argumentName || 'path';
    // console.log("createBrowserParamHistory: ", argumentName);
    //get url param value;

    const argumentValue = argumentName ? new URLSearchParams(window.location.search).get(argumentName) : null;
    // console.log("createBrowserParamHistory initial val: ", argumentValue);
    const history = createMemoryHistory({
        initialEntries: argumentValue? [argumentValue] : ["/"],
        v5Compat: true
    });
    history.listen((data)=>historyListener(data, argumentName));
    return history;
}


export function createBrowserRouter(routes, opts) {
    var _window;
    let { argumentName = 'path', basename = null} = opts;

    return createRouter({
      basename:basename,
      history:createBrowserParamHistory(argumentName),
      hydrationData: (opts == null ? 0 : opts.hydrationData) || ((_window = window) == null ? 0 : _window.__staticRouterHydrationData),
      routes: UNSAFE_enhanceManualRouteObjects(routes)
    }).initialize();
}

//   /**
//    * A <Router> for use in web browsers. Provides the cleanest URLs.
//    */
export function BrowserRouter({ children, window, argumentName }) {
    argumentName = argumentName || 'path';

    let historyRef = React.useRef();
    if (historyRef.current == null) {
        historyRef.current = createBrowserParamHistory(argumentName);
    }
  
    let history = historyRef.current;
    let [state, setState] = React.useState({
      action: history.action,
      location: history.location 
    });

    React.useLayoutEffect(() => history.listen((data)=> {
        historyListener(data, argumentName);
        setState(data)
    }), [history]);
  
    return (
      <Router
        children={children}
        action={state.action}
        location={state.location}
        navigator={history} 
      />
    );
}