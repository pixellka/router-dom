"use strict";
////////////////////////////////////////////////////////////////////////////////
//#region Types and Constants
////////////////////////////////////////////////////////////////////////////////
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parsePath = exports.createPath = exports.createLocation = exports.createHashHistory = exports.createBrowserHistory = exports.createMemoryHistory = exports.Action = void 0;
/**
 * Actions represent the type of change to a location value.
 */
var Action;
(function (Action) {
    /**
     * A POP indicates a change to an arbitrary index in the history stack, such
     * as a back or forward navigation. It does not describe the direction of the
     * navigation, only that the current index changed.
     *
     * Note: This is the default action for newly created history objects.
     */
    Action["Pop"] = "POP";
    /**
     * A PUSH indicates a new entry being added to the history stack, such as when
     * a link is clicked and a new page loads. When this happens, all subsequent
     * entries in the stack are lost.
     */
    Action["Push"] = "PUSH";
    /**
     * A REPLACE indicates the entry at the current index in the history stack
     * being replaced by a new one.
     */
    Action["Replace"] = "REPLACE";
})(Action = exports.Action || (exports.Action = {}));
var PopStateEventType = "popstate";
/**
 * Memory history stores the current location in memory. It is designed for use
 * in stateful non-browser environments like tests and React Native.
 */
function createMemoryHistory(options) {
    if (options === void 0) { options = {}; }
    var _a = options.initialEntries, initialEntries = _a === void 0 ? ["/"] : _a, initialIndex = options.initialIndex, _b = options.v5Compat, v5Compat = _b === void 0 ? false : _b;
    var entries; // Declare so we can access from createMemoryLocation
    entries = initialEntries.map(function (entry, index) {
        return createMemoryLocation(entry, null, index === 0 ? "default" : undefined);
    });
    var index = clampIndex(initialIndex == null ? entries.length - 1 : initialIndex);
    var action = Action.Pop;
    var listener = null;
    function clampIndex(n) {
        return Math.min(Math.max(n, 0), entries.length - 1);
    }
    function getCurrentLocation() {
        return entries[index];
    }
    function createMemoryLocation(to, state, key) {
        if (state === void 0) { state = null; }
        var location = createLocation(entries ? getCurrentLocation().pathname : "/", to, state, key);
        warning(location.pathname.charAt(0) === "/", "relative pathnames are not supported in memory history: ".concat(JSON.stringify(to)));
        return location;
    }
    var history = {
        get index() {
            return index;
        },
        get action() {
            return action;
        },
        get location() {
            return getCurrentLocation();
        },
        createHref: function (to) {
            return typeof to === "string" ? to : createPath(to);
        },
        push: function (to, state) {
            action = Action.Push;
            var nextLocation = createMemoryLocation(to, state);
            index += 1;
            entries.splice(index, entries.length, nextLocation);
            if (v5Compat && listener) {
                listener({ action: action, location: nextLocation });
            }
        },
        replace: function (to, state) {
            action = Action.Replace;
            var nextLocation = createMemoryLocation(to, state);
            entries[index] = nextLocation;
            if (v5Compat && listener) {
                listener({ action: action, location: nextLocation });
            }
        },
        go: function (delta) {
            action = Action.Pop;
            index = clampIndex(index + delta);
            if (listener) {
                listener({ action: action, location: getCurrentLocation() });
            }
        },
        listen: function (fn) {
            listener = fn;
            return function () {
                listener = null;
            };
        },
    };
    return history;
}
exports.createMemoryHistory = createMemoryHistory;
/**
 * Browser history stores the location in regular URLs. This is the standard for
 * most web apps, but it requires some configuration on the server to ensure you
 * serve the same app at multiple URLs.
 *
 * @see https://github.com/remix-run/history/tree/main/docs/api-reference.md#createbrowserhistory
 */
function createBrowserHistory(options, argumentName) {
    if (options === void 0) { options = {}; }
    if (argumentName === void 0) { argumentName = "path"; }
    console.log("createBrowserHistory", options, argumentName);
    function createBrowserLocation(window, globalHistory) {
        var _a, _b;
        //   let { pathname, search, hash } = window.location;
        var _c = window.location, search = _c.search, hash = _c.hash;
        var params = new Map(search.slice(1).split('&').map(function (kv) { return kv.split('='); }));
        //   console.log(params, argumentName);
        var pathname = params.has(argumentName) ? params.get(argumentName) : '/';
        //   console.log("Pathname: ", pathname);
        return createLocation("", { pathname: pathname, search: search, hash: hash }, 
        // state defaults to `null` because `window.history.state` does
        ((_a = globalHistory.state) === null || _a === void 0 ? void 0 : _a.usr) || null, ((_b = globalHistory.state) === null || _b === void 0 ? void 0 : _b.key) || "default");
    }
    function getFullHref(path) {
        var _a = window.location, pathname = _a.pathname, search = _a.search, hash = _a.hash;
        var params = new Map(search.slice(1).split('&').map(function (kv) { return kv.split('='); }));
        params.set(argumentName, path);
        var newSearch = '';
        params.forEach(function (value, key) { newSearch += (newSearch.length ? "&" : '') + "".concat(key, "=").concat(value); });
        return pathname + (newSearch.length ? '?' + newSearch : '') + (hash.length ? '?' + hash : '');
    }
    function createBrowserHref(window, to) {
        var pathStr = typeof to === "string" ? to : to.pathname;
        // console.log("PathStr: ", pathStr);
        return getFullHref(pathStr !== null && pathStr !== void 0 ? pathStr : '/');
        //   return typeof to === "string" ? to : createPath(to);
    }
    return getUrlBasedHistory(createBrowserLocation, createBrowserHref, null, options);
}
exports.createBrowserHistory = createBrowserHistory;
/**
 * Hash history stores the location in window.location.hash. This makes it ideal
 * for situations where you don't want to send the location to the server for
 * some reason, either because you do cannot configure it or the URL space is
 * reserved for something else.
 *
 * @see https://github.com/remix-run/history/tree/main/docs/api-reference.md#createhashhistory
 */
function createHashHistory(options) {
    if (options === void 0) { options = {}; }
    function createHashLocation(window, globalHistory) {
        var _a, _b;
        var _c = parsePath(window.location.hash.substr(1)), _d = _c.pathname, pathname = _d === void 0 ? "/" : _d, _e = _c.search, search = _e === void 0 ? "" : _e, _f = _c.hash, hash = _f === void 0 ? "" : _f;
        return createLocation("", { pathname: pathname, search: search, hash: hash }, 
        // state defaults to `null` because `window.history.state` does
        ((_a = globalHistory.state) === null || _a === void 0 ? void 0 : _a.usr) || null, ((_b = globalHistory.state) === null || _b === void 0 ? void 0 : _b.key) || "default");
    }
    function createHashHref(window, to) {
        var base = window.document.querySelector("base");
        var href = "";
        if (base && base.getAttribute("href")) {
            var url = window.location.href;
            var hashIndex = url.indexOf("#");
            href = hashIndex === -1 ? url : url.slice(0, hashIndex);
        }
        return href + "#" + (typeof to === "string" ? to : createPath(to));
    }
    function validateHashLocation(location, to) {
        warning(location.pathname.charAt(0) === "/", "relative pathnames are not supported in hash history.push(".concat(JSON.stringify(to), ")"));
    }
    return getUrlBasedHistory(createHashLocation, createHashHref, validateHashLocation, options);
}
exports.createHashHistory = createHashHistory;
//#endregion
////////////////////////////////////////////////////////////////////////////////
//#region UTILS
////////////////////////////////////////////////////////////////////////////////
function warning(cond, message) {
    if (!cond) {
        // eslint-disable-next-line no-console
        if (typeof console !== "undefined")
            console.warn(message);
        try {
            // Welcome to debugging history!
            //
            // This error is thrown as a convenience so you can more easily
            // find the source for a warning that appears in the console by
            // enabling "pause on exceptions" in your JavaScript debugger.
            throw new Error(message);
            // eslint-disable-next-line no-empty
        }
        catch (e) { }
    }
}
function createKey() {
    return Math.random().toString(36).substr(2, 8);
}
/**
 * For browser-based histories, we combine the state and key into an object
 */
function getHistoryState(location) {
    return {
        usr: location.state,
        key: location.key,
    };
}
/**
 * Creates a Location object with a unique key from the given Path
 */
function createLocation(current, to, state, key) {
    var _a;
    if (state === void 0) { state = null; }
    var location = __assign(__assign({ pathname: typeof current === "string" ? current : current.pathname, search: "", hash: "" }, (typeof to === "string" ? parsePath(to) : to)), { state: state, 
        // TODO: This could be cleaned up.  push/replace should probably just take
        // full Locations now and avoid the need to run through this flow at all
        // But that's a pretty big refactor to the current test suite so going to
        // keep as is for the time being and just let any incoming keys take precedence
        key: ((_a = to) === null || _a === void 0 ? void 0 : _a.key) || key || createKey() });
    return location;
}
exports.createLocation = createLocation;
/**
 * Creates a string URL path from the given pathname, search, and hash components.
 */
function createPath(_a) {
    var _b = _a.pathname, pathname = _b === void 0 ? "/" : _b, _c = _a.search, search = _c === void 0 ? "" : _c, _d = _a.hash, hash = _d === void 0 ? "" : _d;
    if (search && search !== "?")
        pathname += search.charAt(0) === "?" ? search : "?" + search;
    if (hash && hash !== "#")
        pathname += hash.charAt(0) === "#" ? hash : "#" + hash;
    return pathname;
}
exports.createPath = createPath;
/**
 * Parses a string URL path into its separate pathname, search, and hash components.
 */
function parsePath(path) {
    var parsedPath = {};
    if (path) {
        var hashIndex = path.indexOf("#");
        if (hashIndex >= 0) {
            parsedPath.hash = path.substr(hashIndex);
            path = path.substr(0, hashIndex);
        }
        var searchIndex = path.indexOf("?");
        if (searchIndex >= 0) {
            parsedPath.search = path.substr(searchIndex);
            path = path.substr(0, searchIndex);
        }
        if (path) {
            parsedPath.pathname = path;
        }
    }
    return parsedPath;
}
exports.parsePath = parsePath;
function getUrlBasedHistory(getLocation, createHref, validateLocation, options) {
    if (options === void 0) { options = {}; }
    var _a = options.window, window = _a === void 0 ? document.defaultView : _a, _b = options.v5Compat, v5Compat = _b === void 0 ? false : _b;
    var globalHistory = window.history;
    var action = Action.Pop;
    var listener = null;
    function handlePop() {
        action = Action.Pop;
        if (listener) {
            listener({ action: action, location: history.location });
        }
    }
    function push(to, state) {
        action = Action.Push;
        var location = createLocation(history.location, to, state);
        validateLocation === null || validateLocation === void 0 ? void 0 : validateLocation(location, to);
        var historyState = getHistoryState(location);
        var url = history.createHref(location);
        // try...catch because iOS limits us to 100 pushState calls :/
        try {
            globalHistory.pushState(historyState, "", url);
        }
        catch (error) {
            // They are going to lose state here, but there is no real
            // way to warn them about it since the page will refresh...
            window.location.assign(url);
        }
        if (v5Compat && listener) {
            listener({ action: action, location: location });
        }
    }
    function replace(to, state) {
        action = Action.Replace;
        var location = createLocation(history.location, to, state);
        validateLocation === null || validateLocation === void 0 ? void 0 : validateLocation(location, to);
        var historyState = getHistoryState(location);
        var url = history.createHref(location);
        globalHistory.replaceState(historyState, "", url);
        if (v5Compat && listener) {
            listener({ action: action, location: location });
        }
    }
    var history = {
        get action() {
            return action;
        },
        get location() {
            return getLocation(window, globalHistory);
        },
        listen: function (fn) {
            if (listener) {
                throw new Error("A history only accepts one active listener");
            }
            window.addEventListener(PopStateEventType, handlePop);
            listener = fn;
            return function () {
                window.removeEventListener(PopStateEventType, handlePop);
                listener = null;
            };
        },
        createHref: function (to) {
            return createHref(window, to);
        },
        encodeLocation: function (to) {
            let path = typeof to === "string" ? parsePath(to) : to;
            return {
                pathname: path.pathname || "",
                search: path.search || "",
                hash: path.hash || "",
            }
        },

        push: push,
        replace: replace,
        go: function (n) {
            return globalHistory.go(n);
        },
    };
    return history;
}
//#endregion
