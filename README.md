# Modified React Router v6.61

This is a slightly modified version of react router, which allows to use URL GET param instead of pathname.

Requires prop *argumentName* for both *BrowserRouter* and *createBrowserRouter*.

```
<BrowserRouter argumentName="path">
  ...
</BrowserRouter>
```

```
    const router = createBrowserRouter(routes, {argumentName: "path"});
    ...
    <BrowserProvider router="router"> 
    ...
    </BrowserProvider>
```


Links and Navlinks do still look wrong.

Based on React-router v.6.61